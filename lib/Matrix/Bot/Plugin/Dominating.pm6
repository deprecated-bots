use Config;
use HTTP::UserAgent;
use JSON::Fast;
use Matrix::Bot::Plugin;
use Matrix::Bot::Event;

unit class Matrix::Bot::Plugin::Dominating is Matrix::Bot::Plugin;


has HTTP::UserAgent $!ua;
has Int $!last-updated;
has $!current-player = "d365noscope";
has $.game-id is rw;
has %.config;

has %!matrix-players =:baco<Baco>, :matiaslina<Matias>, :YareYare<huaira>, :nstefani<nico_st>, :fedediaz<fede_diaz>, :d365noscope<Manu>;

submethod BUILD(:%!config, :$!game-id) {
    $!ua = HTTP::UserAgent.new();
    $!last-updated = DateTime.now.posix;
}

method current-matrix-player {
    %!matrix-players{$!current-player} // "$!current-player";
}

method generate-events(Str $message) {
    %.config<rooms>.map(
        -> $room-id { Matrix::Bot::Event.new(:$room-id, :$message) }
    )
}

method current-game {
    $.game-id // %.config<starting_game>
}

multi method handle-room-text($e where *.message.starts-with("!dominating") --> Str) {
    if $e.message ~~ m/"!dominating change-game " $<game-id> = [ \d+ ]/ {
        say "Changing game id from {$.current-game} to {$<game-id>}";
        my $pre-game-id = $!game-id;
        $!game-id = $<game-id>;
        return "Changing game id from {$pre-game-id} to {$<game-id>}";
    }
}

multi method event-handler(--> Supply) {
    my $handler = supply {
        my $last-updated = now.Int;
        loop {
            CATCH {
	        when X::AdHoc {
		    .message.note;
		    sleep 60;
		    next;
	        }
	        default { .^name.say; .resume }
	    }
            my %form = last_update => ~$last-updated;
            my $response = $!ua.post(
                "https://dominating12.com/game/{$.current-game}/play/update-state",
                %form
            );

            if $response.is-success {
                my $data = from-json($response.content);

                $last-updated = $data<now>.Int;

                my %players = $data<players>.values.map({ .<user_id> => .<username> }).List;
                my $turn = %players{$data<turns>.head.<user_id>};

                if $turn.defined && $turn eq "" {
                    say "[{DateTime.now()}] {$.current-matrix-player} won!";
                    emit($_) for self.generate-events("{$.current-matrix-player} gano!");
                    exit 0;
                }

                if $turn.defined && $turn ne $!current-player {
                    say "[{DateTime.now()}] Changing turn from $!current-player to $turn";
                    $!current-player = $turn;
                    if $!current-player ne "" {
                        emit($_) for self.generate-events("Le toca a $.current-matrix-player !");
                    }
                } else {
                    say "[{DateTime.now()}] Still turn of $!current-player";
                }

                sleep 60 * 5;
            } else {
                note "Error response: {$response.status-line}";
            }
        }
    }

    return $handler;
}
