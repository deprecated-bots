use v6.c;
unit class Matrix::Bot::DeprecatedOrg:ver<0.0.1>:auth<cpan:MATIASL>;

use Config;
use Matrix::Bot;
use Matrix::Bot::Plugin::Dominating;


sub MAIN($config-file where *.IO.f) is export {
    my $config = Config.new().read($config-file);

    my %matrix-config = $config<matrix>;
    dd %matrix-config;
    dd $config<dominating>;

    my $bot = Matrix::Bot.new(
        home-server => %matrix-config<server>,
        username => %matrix-config<username>,
        password => %matrix-config<password>,
        plugins => [
                    Matrix::Bot::Plugin::Dominating.new(:config($config<dominating>))
                ],
    );

    $bot.run;
}

=begin pod

=head1 NAME

Matrix::Bot::DeprecatedOrg - blah blah blah

=head1 SYNOPSIS

=begin code :lang<perl6>

use Matrix::Bot::DeprecatedOrg;

=end code

=head1 DESCRIPTION

Matrix::Bot::DeprecatedOrg is ...

=head1 AUTHOR

Matias Linares <matiaslina@gmail.com>

=head1 COPYRIGHT AND LICENSE

Copyright 2020 Matias Linares

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod
