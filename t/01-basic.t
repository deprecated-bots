use v6.c;

use Test;
use Matrix::Bot::Plugin::Dominating;

plan 2;


ok Matrix::Bot::Plugin::Dominating.new(game-id => 1, config => %());

my \Dominating = Matrix::Bot::Plugin::Dominating;

is Dominating.new(:config(a => 1)).config<a>, 1;


done-testing;
