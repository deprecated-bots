NAME
====

Matrix::Bot::DeprecatedOrg - blah blah blah

SYNOPSIS
========

```perl6
use Matrix::Bot::DeprecatedOrg;
```

DESCRIPTION
===========

Matrix::Bot::DeprecatedOrg is ...

AUTHOR
======

Matias Linares <matiaslina@gmail.com>

COPYRIGHT AND LICENSE
=====================

Copyright 2020 Matias Linares

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

